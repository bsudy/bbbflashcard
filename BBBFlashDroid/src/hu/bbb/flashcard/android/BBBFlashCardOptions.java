package hu.bbb.flashcard.android;

import hu.bbb.flashcard.logic.FirstToLastIterator;
import hu.bbb.flashcard.logic.IteratorInterface;
import hu.bbb.flashcard.model.FlashCard;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class BBBFlashCardOptions extends Activity {
	
    /** Called when the activity is first created. */

	IteratorInterface mCardIterator = new FirstToLastIterator(false, new FlashCard[] { 
			new FlashCard("apple", "alma"), 
			new FlashCard("plum", "szilva"),
			new FlashCard("pear", "korte"),
			new FlashCard("blueberry", "fekete afonya")
	});
	
	FlashCard mActiveCard;
	private static final String TAG = BBBFlashCardOptions.class.getName();
	TextView mCardViewA;
	TextView mCardViewB;
	TextView mNextCardViewA;
	TextView mNextCardViewB;
	ImageButton mRightButton;
	ImageButton mWrongButton;
	ImageButton mOtherSideButton;

	private final static int SCALE_DURATION = 100;
	private final static int ROTATE_DURATION = 500;
	private final static int ANIM_DURATION = 600;
	

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.practice_view);
        mCardViewA = (TextView)findViewById(R.id.cardContentView);
        mCardViewB = (TextView)findViewById(R.id.cardContentViewB);
        mNextCardViewA = (TextView)findViewById(R.id.nextCardContentView);
        mNextCardViewB = (TextView)findViewById(R.id.nextCardContentViewB);
    	try {
    		this.mActiveCard = this.mCardIterator.getNext(false, null);
    	} catch (final Exception e) {
    		Log.e(TAG, "Iterator error.", e);
    	}
    	fillCardView();
    	
        mRightButton = (ImageButton)findViewById(R.id.rightAnswerButton);
        mRightButton.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		BBBFlashCardOptions.this.rightButtonClicked();
        	}
        });

        mWrongButton = (ImageButton)findViewById(R.id.wrongAnswerButton);
        mWrongButton.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		BBBFlashCardOptions.this.wrongButtonClicked();
        	}
        });

        mOtherSideButton = (ImageButton)findViewById(R.id.otherSideButton);
        mOtherSideButton.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		BBBFlashCardOptions.this.otherSideButtonClicked();
        	}
        });
        
        Button dashboardButton = (Button)findViewById(R.id.backToDashboardButton);
        dashboardButton.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		Toast.makeText(BBBFlashCardOptions.this, "Back to dashboard: not implemented yet!", Toast.LENGTH_SHORT).show();
        	}
        });
    }
    
    public boolean onCreateOptionsMenu(Menu menu) {
    	getMenuInflater().inflate(R.menu.practice_menu, menu);
    	return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	Toast.makeText(this, "Menu selected", Toast.LENGTH_SHORT).show();
    	return true;
    }
    
    private void wrongButtonClicked() {
    	nextCard(false);
    }
    
    private void otherSideButtonClicked() {
    	flipCard();
    }

    private void rightButtonClicked() {
    	nextCard(true);
    }
    
    private void enableButtons(boolean enabled) {
    	mRightButton.setEnabled(enabled);
    	mWrongButton.setEnabled(enabled);
    	mOtherSideButton.setEnabled(enabled);
    }

    private void fillCardView() {
    	mCardViewA.setText(mActiveCard.getSideA());
    	mCardViewB.setText(mActiveCard.getSideB());
    }
    
    private void fillNextCardView() {
    	mNextCardViewA.setText(mActiveCard.getSideA());
    	mNextCardViewB.setText(mActiveCard.getSideB());
    	
    }
    
    private void nextCard(boolean success) {
    	try {
    		this.mActiveCard = this.mCardIterator.getNext(success, null);
    	} catch (final Exception e) {
    		Log.e(TAG, "Iterator error.", e);
    	}
    	fillNextCardView();
    	animateNextCard();
    }
    
    private void animateNextCard() {
    	enableButtons(false);
    	Animation animation = AnimationUtils.loadAnimation(this, R.anim.next_card_animation);
    	animation.setAnimationListener(new AnimationListener() {
    		public void onAnimationEnd(Animation animation) {
    			BBBFlashCardOptions.this.finishNextAnimation();
    		}

			public void onAnimationStart(Animation animation) {}

			public void onAnimationRepeat(Animation animation) {}
    	});
    	Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.next_card_back_animation);
    	mCardViewA.startAnimation(animation);
    	mNextCardViewA.startAnimation(animation2);
    	mNextCardViewA.setVisibility(View.VISIBLE);
    }
    
    private void finishNextAnimation() {
    	mCardViewA.setVisibility(View.GONE);
    	TextView tmp = mCardViewA;
    	mCardViewA = mNextCardViewA;
    	mNextCardViewA = tmp;
    	tmp = mCardViewB;
    	mCardViewB = mNextCardViewB;
    	mNextCardViewB = tmp;
    	enableButtons(true);
    }
    
    private void flipCard() {
    	enableButtons(false);
    	
    	AnimationSet a = new AnimationSet(true);
    	a.setDuration(ANIM_DURATION);
    	a.setInterpolator(new AccelerateInterpolator());
    	a.setAnimationListener(new AnimationListener() {
			public void onAnimationEnd(Animation animation) {
				BBBFlashCardOptions.this.endFlipCard();
			}
			public void onAnimationStart(Animation animation) {}
			public void onAnimationRepeat(Animation animation) {}
    	});
    	
    	a.addAnimation(createScaleAnimation(true));
    	a.addAnimation(creteFlipAnimation(mCardViewA, true));
    	
    	mCardViewA.startAnimation(a);
    }
    
    private void endFlipCard() {
    	View v = mCardViewB;
    	mCardViewA.setVisibility(View.GONE);
    	mCardViewB.setVisibility(View.VISIBLE);
    	mCardViewB.requestFocus();
    	
    	AnimationSet a = new AnimationSet(true);
    	a.setDuration(ANIM_DURATION);
    	a.setInterpolator(new DecelerateInterpolator());
    	a.setAnimationListener(new AnimationListener() {
			public void onAnimationEnd(Animation animation) {
				BBBFlashCardOptions.this.switchCardViews();
			}
			public void onAnimationStart(Animation animation) {}
			public void onAnimationRepeat(Animation animation) {}
    	});
    	
    	a.addAnimation(createScaleAnimation(false));
    	a.addAnimation(creteFlipAnimation(mCardViewB, false));
    	
    	v.startAnimation(a);
    }
    
    private Flip3DAnimation creteFlipAnimation(View v, boolean front) {
    	final float centerX = v.getWidth() / 2f;
    	final float centerY = v.getHeight() / 2f;
    	
    	final Flip3DAnimation rotation;
    	if (front)
    		rotation = new Flip3DAnimation(0, 90, centerX, centerY);
    	else
    		rotation = new Flip3DAnimation(-90, 0, centerX, centerY);
    	rotation.setFillAfter(true);
    	rotation.setDuration(SCALE_DURATION);
    	rotation.setStartOffset(0);
    	
    	return rotation;
    }
    
    private ScaleAnimation createScaleAnimation(boolean front) {
    	final ScaleAnimation animation;
    	if (front)
    		animation = new ScaleAnimation(1f, 0.65f, 1f, 0.65f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
    	else
    		animation = new ScaleAnimation(0.65f, 1f, 0.65f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
    	animation.setFillAfter(false);
    	animation.setDuration(ROTATE_DURATION);
    	animation.setStartOffset(0);
    	
    	return animation;
    }
    
    
    private void switchCardViews() {
    	TextView tmp = mCardViewB;
    	mCardViewB = mCardViewA;
    	mCardViewA = tmp;
    	enableButtons(true);
    }
}