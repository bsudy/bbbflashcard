package hu.bbb.flashcard.model;

import java.util.Collection;

public class FlashCardCollection {

	private final Collection<FlashCard> flashCards;
	private final String name;
	private final String description;

	public FlashCardCollection(final Collection<FlashCard> flashCards, final String name, final String description) {
		this.flashCards = flashCards;
		this.name = name;
		this.description = description;
	}

	public Collection<FlashCard> getFlashCards() {
		return this.flashCards;
	}
	public String getName() {
		return this.name;
	}
	public String getDescription() {
		return this.description;
	}




}
