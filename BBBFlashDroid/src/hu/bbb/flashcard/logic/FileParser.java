/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard.logic;

import hu.bbb.flashcard.model.FlashCard;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 *
 * @author envagyok
 */
public class FileParser {

    public static Vector parseXML(final String fileName) throws IOException, XmlPullParserException {
        final File filecon = new File(fileName);
        return parseXML(filecon);
    }

    public static Vector parseXML(final File filecon) throws IOException, XmlPullParserException {
        final Vector ret = new Vector();
        if (filecon.exists() && filecon.canRead()) {

            final InputStream fis = new FileInputStream(filecon);
            final XmlPullParser kXmlParser =  XmlPullParserFactory.newInstance().newPullParser();
            kXmlParser.setInput(fis, "UTF-8");
            kXmlParser.next();
            final String rootName = kXmlParser.getName();
            String sideA = null;
            String sideB = null;
            int i = 0;
            while ((!rootName.equalsIgnoreCase(kXmlParser.getName()) || kXmlParser.getEventType() != XmlPullParser.END_TAG) && i < 100) {

                if ("flashCard".equalsIgnoreCase(kXmlParser.getName()) && kXmlParser.getEventType() == XmlPullParser.END_TAG) {
                    i = 0;
                    System.out.println("Add card: SideA: " + sideA + " sideB: " + sideB);
                    if (sideA != null && sideB != null) {
                        ret.addElement(new FlashCard(sideA, sideB));
                    }
                    sideA = null;
                    sideB = null;
                }
                if ("sideA".equalsIgnoreCase(kXmlParser.getName()) && kXmlParser.getEventType() == XmlPullParser.START_TAG) {
                    sideA = kXmlParser.nextText();
                    i = 0;
                }
                if ("sideB".equalsIgnoreCase(kXmlParser.getName()) && kXmlParser.getEventType() == XmlPullParser.START_TAG) {
                    sideB = kXmlParser.nextText();
                    i = 0;
                }
                kXmlParser.next();
                i++;
            }
            System.out.println("alma");

        }


        return ret;
    }


}
