/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard.logic;

import hu.bbb.flashcard.dao.DAO;
import hu.bbb.flashcard.dao.DAOException;
import hu.bbb.flashcard.dao.ObjectSet;
import hu.bbb.flashcard.model.FlashCard;
import java.util.Enumeration;
import java.util.Vector;
//import net.sourceforge.floggy.persistence.FloggyException;
//import net.sourceforge.floggy.persistence.ObjectSet;
//import net.sourceforge.floggy.persistence.PersistableManager;

/**
 *
 * @author bsudy
 */
public class LeitnerIterator implements IteratorInterface {

    private final boolean reverse;
    private FlashCard lastCard = null;
    private Vector sessionCards = new Vector();
    private int session = 14;

    private int numberOfCards = 0;
    private int sumOfSessions = 0;

    private Vector[][] decks = new Vector[5][14];

    public LeitnerIterator(boolean reverse, FlashCard[] flashcards) {
        this.reverse = reverse;
        initDecks();
        init(flashcards);
    }

    public LeitnerIterator(boolean reverse, ObjectSet flashcards) throws DAOException {
        this.reverse = reverse;
        initDecks();
        init(flashcards);
    }

    private void initDecks() {
        for(int i = 0; i < 5; i++) {
            int a = getDeckSize(i);
            for (int j = 0; j < a; j++) {
                decks[i][j] = new Vector();
            }
        }
    }

    private int getDeckSize(int deck) {
        int a;
        switch (deck) {
            case 4: a = 14; break;
            case 3: a = 8; break;
            case 2: a = 4; break;
            case 1: a = 2; break;
            default: a = 1;
        }
        return a;
    }

    private int takeCard(FlashCard card) {
        int deck = card.getDeck(reverse);
        if (deck < 5) {
            int a;
            switch (deck) {
                case 4: a = 14; break;
                case 3: a = 8; break;
                case 2: a = 4; break;
                case 1: a = 2; break;
                default: a = 1;
            }
            int bucket = card.getLastSuccess(reverse) % a;
            decks[deck][bucket].addElement(card);
        }
        return deck;
    }


    private void init(FlashCard[] flashcards) {
        for(int i = 0; i < flashcards.length; i++) {
            numberOfCards ++;
            sumOfSessions += takeCard(flashcards[i]);
        }
    }

    private void init(ObjectSet flashcards) throws DAOException {
        for(int i = 0; i < flashcards.size(); i++) {
            numberOfCards ++;
            sumOfSessions += takeCard((FlashCard) flashcards.get(i));
        }
    }

    private void newSession() {
        session++;
        sessionCards.removeAllElements();
        for (int i=0; i<5; i++) {
            int a = getDeckSize(i);
            int bucket = session % a;
            Enumeration enumeration = decks[i][bucket].elements();
            while (enumeration.hasMoreElements()) {
                sessionCards.addElement(enumeration.nextElement());
            }
        }

    }

    private boolean hasCard() {
        for(int i = 0; i < 5; i++) {
            int a = getDeckSize(i);
            for (int j = 0; j < a; j++) {
                if (decks[i][j].size() > 0)
                    return true;
            }
        }
        return false;
    }

    public FlashCard getNext(boolean success, DAO pm) throws DAOException {
        if (lastCard != null) {
            //Remove from bucket
            int deck = lastCard.getDeck(reverse);
            int a = getDeckSize(deck);
            int bucket = (session) % a;
            decks[deck][bucket].removeElement(lastCard);

            sumOfSessions -= deck;
            if (success) {
                lastCard.success(session - 1, reverse);
            } else {
                lastCard.failure(reverse);
            }
            deck = takeCard(lastCard);
            sumOfSessions += deck;
            pm.save(lastCard);
        }
        if (hasCard()) {
            while (sessionCards == null || !(sessionCards.size() > 0)) {
                newSession();
            }
            lastCard =  (FlashCard) sessionCards.firstElement();
            sessionCards.removeElement(lastCard);

            return lastCard;
        } else {
            return null;
        }
    }

    public String getInformation() {
        //return sumOfSessions + " %";
        return (100 * sumOfSessions / (5 * numberOfCards)) + " % Mastery: " + (lastCard.getDeck(reverse) + 1);
    }

}
