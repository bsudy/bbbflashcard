/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard.logic;

import hu.bbb.flashcard.dao.DAO;
import hu.bbb.flashcard.dao.DAOException;
import hu.bbb.flashcard.dao.ObjectSet;
import hu.bbb.flashcard.model.FlashCard;

/**
 *
 * @author Barnabas Sudy 2011
 */
public class FirstToLastIterator implements IteratorInterface {

    private final boolean reverse;
    private final FlashCard[] cards;
    private int lastElement = -1;

    public FirstToLastIterator(boolean reverse, FlashCard[] flashcards) {
        this.reverse = reverse;
        this.cards = flashcards;
    }

    public FirstToLastIterator(boolean reverse, ObjectSet flashcards) throws DAOException {
        this.reverse = reverse;
        cards = new FlashCard[flashcards.size()];
        for(int i = 0; i < flashcards.size(); i++) {
            cards[i] = (FlashCard) flashcards.get(i);
        }
    }

    private int getNextIndex() {
        if (reverse) {
            lastElement--;
        } else {
            lastElement++;
        }
        if (lastElement < 0) {
            lastElement = cards.length - 1;
        } else if (lastElement >= cards.length) {
            lastElement = 0;
        }

        return lastElement;
    }

    public FlashCard getNext(boolean success, DAO pm) throws DAOException {
        return cards[getNextIndex()];
    }

    public String getInformation() {
        return "";
    }

}
