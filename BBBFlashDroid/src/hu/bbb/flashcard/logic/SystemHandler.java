/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard.logic;

//import net.sourceforge.floggy.persistence.FloggyException;

import hu.bbb.flashcard.dao.DAOException;
import hu.bbb.flashcard.dao.ObjectSet;

//import net.sourceforge.floggy.persistence.ObjectSet;

/**
 *
 * @author Barnabas Sudy 2011
 */
public class SystemHandler {

    private static int MAX_NUMBER = 1;

    public static String getName(int i) {
        switch (i) {
            case 0: return "Leitner";
            case 1: return "First to Last";
            default: return "N/A";
        }
    }

    public static int getNext(int i) {
        i++;
        if (i > MAX_NUMBER) {
            i = 0;
        }
        return i;
    }

    public static IteratorInterface getIterator(int i, boolean reverse, ObjectSet flashcards) throws DAOException {
        switch (i) {
            case 0: return new LeitnerIterator(reverse, flashcards);
            case 1: return new FirstToLastIterator(reverse, flashcards);
            default: throw new IllegalArgumentException("No learning system is related to this Id!");
        }
    }


}
