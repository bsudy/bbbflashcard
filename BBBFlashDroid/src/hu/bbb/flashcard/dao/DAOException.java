/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hu.bbb.flashcard.dao;

/**
 *
 * @author bsudy
 */
public class DAOException extends Exception {

    private final Exception e;

    public DAOException(Exception e) {
        this.e = e;
    }

    public String getMessage() {
        return e.getMessage();
    }

    public void printStackTrace() {
        e.printStackTrace();
    }

    public String toString() {
        return e.toString();
    }








}
