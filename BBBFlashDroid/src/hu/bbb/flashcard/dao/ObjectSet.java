/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hu.bbb.flashcard.dao;

/**
 *
 * @author bsudy
 */
public interface ObjectSet {

    int size();

    Object get(int i) throws DAOException;

}
