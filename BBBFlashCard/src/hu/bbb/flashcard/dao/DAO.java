/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard.dao;

import hu.bbb.flashcard.model.FlashCard;
import java.util.Vector;


/**
 *
 * @author bsudy
 */
public interface DAO {

    public Vector loadCards() throws DAOException;

    public void save(FlashCard object) throws DAOException;

    public void delete(FlashCard flashcard) throws DAOException;

    public void deleteAll() throws DAOException;


}
