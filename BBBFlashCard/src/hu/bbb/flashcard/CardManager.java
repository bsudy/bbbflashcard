/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard;

import hu.bbb.flashcard.dao.DAO;
import hu.bbb.flashcard.dao.DAOException;
import hu.bbb.flashcard.daome.DAOME;
import hu.bbb.flashcard.model.FlashCard;
import java.util.Vector;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Gauge;

/**
 *
 * @author envagyok
 */
public class CardManager {

    private final DAO pm = new DAOME();
//    private DisplayController displayController;

    private Vector cards;

    public CardManager() {
        
    }

//    public void init (DisplayController displayController) {
//        this.displayController = displayController;
//    }

    public Vector getCards() throws DAOException {
        if (cards == null) {
            loadCards();
        }
        return cards;
    }

    private void loadCards() throws DAOException {
        cards = new Vector();
//        Display display = displayController.getDisplay();
//        Displayable displayable = display.getCurrent();
//        Form form = new Form("Loading");
//        Gauge gauge = new Gauge("Loading...", false, Gauge.INDEFINITE,
//                Gauge.CONTINUOUS_RUNNING);
//        gauge.setLayout(Gauge.LAYOUT_CENTER);
//        form.append(gauge);
//        display.setCurrent(form);
//
        cards = pm.loadCards();
//        display.setCurrent(displayable);
//        Loading.load(displayController.getDisplay(), new LoadCards(displayController.getDisplay().getCurrent()));
    }

//    class LoadCards implements LoadableInterface {
//
//        private final Displayable nextDisplayable;
//
//        LoadCards(final Displayable nextDisplayable) {
//            this.nextDisplayable = nextDisplayable;
//        }
//
//        public void run() throws Exception {
//            cards = new Vector();
//            cards = pm.loadCards();
//        }
//
//        public void afterRun(Exception ex) {
//            if (ex == null) {
//                displayController.getDisplay().setCurrent(nextDisplayable);
//            } else {
//                displayController.showUnknownError(ex);
//            }
//        }
//
//    }

    public void removeCard(FlashCard card) throws DAOException {
        pm.delete(card);
        cards.removeElement(card);
    }

    public void saveCard(FlashCard card) throws DAOException {
        if (cards == null) {
            loadCards();
        }
        pm.save(card);
        cards.addElement(card);
    }

    void deleteAllCard() throws DAOException {
        pm.deleteAll();
        if (cards != null) {
            cards.removeAllElements();
        }
    }

    void deleteCard(FlashCard flashCard) throws DAOException {
        pm.delete(flashCard);
        cards.removeElement(flashCard);
    }
}
