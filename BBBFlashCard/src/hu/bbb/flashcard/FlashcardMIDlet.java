/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard;

import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;

public final class FlashcardMIDlet extends MIDlet {
    
    private final DisplayController displayController;
    private final CardManager cardManager;
    private final Practice practice;
    

    public FlashcardMIDlet() {

        cardManager = new CardManager();
        displayController = new DisplayController(this, cardManager);
        practice = new Practice(getDisplay(), displayController, cardManager);
//        cardManager.init(displayController);
    }

 

    public void startApp() {
        displayController.showOptions();
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
    }

    /**
     * Returns a display instance.
     * @return the display instance.
     */
    public final Display getDisplay() {
        return Display.getDisplay(this);
    }

    CardManager getCardManager() {
        return cardManager;
    }

    public void startPractice(final int systemId, final boolean reverse) {
        practice.startPractice(systemId, reverse);
    }


//    public void reset() throws DAOException {
//        pm.deleteAll(FlashCard.class);
//        pm.save(new FlashCard("Apple", "alma"));
//        pm.save(new FlashCard("Pear", "körte"));
//        pm.save(new FlashCard("Plum", "szilva"));
//        pm.save(new FlashCard("Peach", "öszibarack"));
//        pm.save(new FlashCard("Blackberry", "áfonya"));
//        pm.save(new FlashCard("Strawberry", "eper"));
//
//        String[] recordStores = RecordStore.listRecordStores();
//        if (recordStores != null) {
//            for (int i = 0; i < recordStores.length; i++) {
//                System.out.println("RecordStore:" + recordStores[i]);
//            }
//        }
//
//        ObjectSet flashCards = pm.find(FlashCard.class);
//        for (int i = 0; i < flashCards.size(); i++) {
//            FlashCard card = (FlashCard) flashCards.get(i);
//            System.out.println("FlashCard: sideA: " + card.getSideA() + " sideB: " + card.getSideB());
//        }
//
//    }

}
