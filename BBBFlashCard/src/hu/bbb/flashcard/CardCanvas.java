/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author bsudy
 */
public class CardCanvas extends Canvas{

    private String text = "";
    int percentage = 0;

    public CardCanvas() {

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    protected void paint(Graphics g) {
        int width = getWidth();
        int height = getHeight();
        g.setColor(0xffffff);
        g.fillRect(0, 0, width, height);

        Font font = g.getFont();
        int fontHeight = font.getHeight();
        int fontWidth = font.stringWidth(text);
        g.setColor(255, 0, 0);
        g.setFont(font);

        g.drawString(text, (width - fontWidth) /2, (height - fontHeight) / 2, g.TOP|g.LEFT);

    }

}
