/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard.logic;

import hu.bbb.flashcard.CardManager;
import hu.bbb.flashcard.dao.DAOException;
import hu.bbb.flashcard.model.FlashCard;
import javax.microedition.lcdui.Displayable;
//import net.sourceforge.floggy.persistence.FloggyException;
//import net.sourceforge.floggy.persistence.PersistableManager;

/**
 *
 * @author bsudy
 */
public interface IteratorInterface {

    FlashCard getNext(boolean success, CardManager cardManager) throws DAOException;

    String getInformation();

    boolean isStatistic();

    Displayable statistic();
}
