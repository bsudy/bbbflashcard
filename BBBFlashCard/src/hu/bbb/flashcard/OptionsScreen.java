/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard;

import hu.bbb.flashcard.logic.SystemHandler;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 *
 * @author envagyok
 */
public class OptionsScreen extends List implements CommandListener {


    private final Command exitCommand   = new Command("Exit", Command.EXIT, 99);
    private final Command selectCommand = new Command("Select", Command.ITEM, 1);

    private final DisplayController displayController;
    private final FlashcardMIDlet flashcardMIDlet;

    private int systemId = 0;
    private boolean reverse = false;
    private int selectedIndex = 0;

    public OptionsScreen(final DisplayController displayController, final FlashcardMIDlet flashcardMIDlet) {
        super("Options", List.IMPLICIT);
        append("Practice", null);
        append("System: " + SystemHandler.getName(systemId), null);
        append("Direction: " + (reverse ? "Reverse" : "Straight"), null);
        append("Load from file...", null);
        append("Append from file...", null);
        append("Remove the known cards...", null);
//        append("Test Data", null);
        setSelectedIndex(selectedIndex, true);
        setCommandListener(this);
        addCommand(selectCommand);
        addCommand(exitCommand);

        this.displayController = displayController;
        this.flashcardMIDlet = flashcardMIDlet;
    }

    public void commandAction(Command c, Displayable d) {
        if (c == exitCommand) {
            flashcardMIDlet.destroyApp(false);
            flashcardMIDlet.notifyDestroyed();
        } else if (c == selectCommand) {
            switch (getSelectedIndex()) {
                case 0:
                    flashcardMIDlet.startPractice(systemId, reverse);
                    break;
                case 1:
                    systemId = SystemHandler.getNext(systemId);
                    set(1, "System: " + SystemHandler.getName(systemId), null);
                    displayController.showOptions();
                    break;
                case 2:
                    reverse = !reverse;
                    set(2, "Direction: " + (reverse ? "Reverse" : "Straight"), null);
                    displayController.showOptions();
                    break;
                case 3:
                    //reset = true;
                    displayController.showFileBrowser(true);
                    break;
                case 4:
                    //reset = false;
                    displayController.showFileBrowser(false);
                    break;
                case 5:
                    displayController.showRemoveScreen();
                    break;
//                case 6:
//                    try {
//                        reset();
//                    } catch(DAOException ex) {
//                        showAlert("Database error!", "Sorry! Database excpetion has occured!", getOptions(), ex);
//                    }
//                    break;
            }
        }
    }

}
