/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard;

import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Gauge;

/**
 *
 * @author Barnabas Südy 2011
 */
public final class Loading implements Runnable {

    private final static int MAX_VALUE = 10;

    private Form form;
    private Gauge gauge;
    private LoadableInterface runnable;

    private Loading(Display display, LoadableInterface runnable) {
        form = new Form("Loading");
        gauge = new Gauge("Loading...", false, Gauge.INDEFINITE,
                Gauge.CONTINUOUS_RUNNING);
        gauge.setLayout(Gauge.LAYOUT_CENTER);
        form.append(gauge);
        display.setCurrent(form);

        this.runnable = runnable;

        //this.run();
    }

    public static void load(Display display, LoadableInterface runnable) {
//        Displayable oldDisplayable = display.getCurrent();
        Loading loading = new Loading(display, runnable);
        Thread thread = new Thread(loading);
        thread.start();

    }

    public void run() {
        Exception ex = null;
        try {
            runnable.run();
        } catch (Exception e) {
            ex = e;
        }
        runnable.afterRun(ex);
    }

    



}
