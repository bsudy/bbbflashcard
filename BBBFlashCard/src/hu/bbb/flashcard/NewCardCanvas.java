/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard;

import com.astrientlabs.text.LineEnumeration;
import java.util.Enumeration;
import java.util.Vector;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Barnabás Südy 2011
 */
public class NewCardCanvas extends Canvas{

    private String text = "";
    private String information = "";

    public NewCardCanvas() {
        
    }

    public void setProperties(final String text, final String information) {
        this.text = text;
        this.information = information;
    }

    protected void paint(Graphics g) {

        int width = getWidth();
        int height = getHeight();

        g.setColor(0xffffff);
        g.fillRect(0, 0, width, height);

        Font myFont = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_MEDIUM);
        g.setFont(myFont);
        g.setColor(0, 0, 255);

        String tmpText = information;
        g.drawString(tmpText, (width - myFont.stringWidth(tmpText)) /2, 0, Graphics.TOP | Graphics.LEFT);

        g.setColor(255, 0, 0);
        
        
        LineEnumeration lineEnumeration = new LineEnumeration(myFont, text, getWidth());
        Vector rows = new Vector();
        while ( lineEnumeration.hasMoreElements() ) {
            rows.addElement(lineEnumeration.nextElement());
        }

        int fontHeight = myFont.getHeight();

        int textHeight = rows.size() * fontHeight;
        int startY = (height - textHeight) / 2;
        

        Enumeration rowEnumeration = rows.elements();
        while ( rowEnumeration.hasMoreElements() ) {

            tmpText = rowEnumeration.nextElement().toString();
            int fontWidth = myFont.stringWidth(tmpText);
            int startX = (width - fontWidth) /2;
            g.drawString(tmpText, startX,startY, Graphics.TOP | Graphics.LEFT);
            startY += myFont.getHeight();

        }
    }

    

}
