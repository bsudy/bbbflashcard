/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard.daome;

import hu.bbb.flashcard.dao.DAO;
import hu.bbb.flashcard.dao.DAOException;
import hu.bbb.flashcard.dao.ObjectSet;
import hu.bbb.flashcard.model.FlashCard;
import java.util.Vector;
import net.sourceforge.floggy.persistence.FloggyException;
import net.sourceforge.floggy.persistence.Persistable;
import net.sourceforge.floggy.persistence.PersistableManager;

/**
 *
 * @author bsudy
 */
public class DAOME implements DAO {

    PersistableManager pm = PersistableManager.getInstance();

    public DAOME() {

    }



    public void save(FlashCard object) throws DAOException {
        try {
            pm.save((Persistable) object);
        } catch (FloggyException ex) {
            throw new DAOException(ex);
        }

    }

    public Vector loadCards() throws DAOException {
        ObjectSet result = find(FlashCard.class);
        Vector flashCards = new Vector();
        for(int i = 0; i < result.size(); i++) {
            flashCards.addElement(result.get(i));
        }
        return flashCards;

    }

    public ObjectSet find(Class type) throws DAOException {
        try {
            return new ObjectSetME(pm.find(type, null, null));
        } catch (FloggyException ex) {
            throw new DAOException(ex);
        }
    }

    public void deleteAll() throws DAOException {
        try {
            pm.deleteAll(FlashCard.class);
        } catch (FloggyException ex) {
            throw new DAOException(ex);
        }
    }

    public void delete(FlashCard flashcard) throws DAOException {
        try {
            pm.delete(flashcard);
        } catch (FloggyException ex) {
            throw new DAOException(ex);
        }
    }

}
