/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard;

import hu.bbb.flashcard.dao.DAOException;
import hu.bbb.flashcard.logic.IteratorInterface;
import hu.bbb.flashcard.logic.SystemHandler;
import hu.bbb.flashcard.model.FlashCard;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;

/**
 *
 * @author envagyok
 */
public class Practice implements CommandListener {

    private final Display display;
    private final DisplayController displayController;
    private final CardManager cardManager;
    private NewCardCanvas firstCanvas = new NewCardCanvas();
    private NewCardCanvas secondCanvas = new NewCardCanvas();

    private Command statisticsCommand = new Command("Statistics", Command.BACK, 99);
    private Command optionsCommand = new Command("Options", Command.BACK, 99);
    private Command otherSiteCommand = new Command("OtherSide", Command.OK, 1);

    private Command successCommand = new Command("Success", Command.OK, 99);
    private Command failureCommand = new Command("Failure", Command.CANCEL, 1);

    private Command backCommand = new Command("Back", Command.BACK, 99);
    
    private int systemId;
    private boolean reverse;

    private IteratorInterface iterator;
    private FlashCard activeCard;

    public Practice(final Display display, final DisplayController displayController, final CardManager cardManager) {
        this.display = display;
        this.displayController = displayController;
        this.cardManager = cardManager;


        firstCanvas.addCommand(otherSiteCommand);
        firstCanvas.setCommandListener(this);

        secondCanvas.addCommand(successCommand);
        secondCanvas.addCommand(failureCommand);
        secondCanvas.setCommandListener(this);
    }

    private Display getDisplay() {
        return display;
    }

    public void startPractice(final int systemId, final boolean reverse) {
        Loading.load(getDisplay(), new StartPractice(systemId, reverse));
    }

    public void showQuestion(boolean success) throws DAOException {
        activeCard = iterator.getNext(success, cardManager);
        if (activeCard != null) {
            display.setCurrent(firstCanvas);
            firstCanvas.setProperties(reverse ? activeCard.getSideB() : activeCard.getSideA(), iterator.getInformation());
            firstCanvas.repaint();
        } else {
            Alert alert = new Alert("Congratulation", "Congratulation! You know all the words!", null, AlertType.INFO);
            alert.setTimeout(3000);
            getDisplay().setCurrent(alert, displayController.getOptions());
        }
    }

    public void showAnswer() {
        display.setCurrent(secondCanvas);
        secondCanvas.setProperties(reverse ? activeCard.getSideA() : activeCard.getSideB(), iterator.getInformation());
        secondCanvas.repaint();
    }

    class StartPractice implements LoadableInterface {

        public StartPractice(final int systemIdi, final boolean reversei) {
            systemId = systemIdi;
            reverse = reversei;
        }

        public void run() throws DAOException {
            iterator = SystemHandler.getIterator(systemId, reverse, cardManager.getCards());
            if (iterator.isStatistic()) {
                firstCanvas.addCommand(statisticsCommand);
            } else {
                firstCanvas.addCommand(optionsCommand);
            }
        }

        public void afterRun(Exception exception) {
            try {
                if (exception != null) {
                    throw exception;
                }
                showQuestion(true);
            } catch (ArrayIndexOutOfBoundsException ex) {
                displayController.showAlert("No card found!", "Load cards before practice!");
            } catch (Exception ex) {
                displayController.showUnknownError(ex);
            }
        }
    }

        public void commandAction(Command c, Displayable s) {
        if (c == otherSiteCommand) {
            showAnswer();
        } else if (c == successCommand) {
            try {
                showQuestion(true);
            } catch (DAOException ex) {
                displayController.showUnknownError(ex);
            }
        } else if (c == failureCommand) {
            try {
                showQuestion(false);
            } catch (DAOException ex) {
                displayController.showUnknownError(ex);
            }
        } else if (c == statisticsCommand) {
            Displayable displayable = iterator.statistic();
            displayable.addCommand(backCommand);
            displayable.addCommand(optionsCommand);
            displayable.setCommandListener(this);
            display.setCurrent(displayable);
        } else if (c == backCommand) {
            display.setCurrent(firstCanvas);
        } else if (c == optionsCommand) {
//            fileBrowser = null;
            displayController.showOptions();
        }
    }

}
