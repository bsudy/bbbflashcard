/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard;

import hu.bbb.flashcard.dao.DAOException;
import hu.bbb.flashcard.model.FlashCard;
import java.util.Enumeration;
import java.util.Vector;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 *
 * @author envagyok
 */
public class RemoveScreen extends List implements CommandListener {

    private final Command backCommand = new Command("Back", Command.BACK, 1);
    private final Command removeCommand = new Command("Remove...", Command.ITEM, 99);
    private final DisplayController displayController;
    private final Display display;
    private final CardManager pm;
    private Vector knownCards = new Vector();
    private Boolean mode;

    public RemoveScreen(Display display, DisplayController displayController, CardManager pm) {
        super("Remove", List.IMPLICIT);
        append("Known", null);
        append("Straight Known", null);
        append("Reverse Known", null);
        addCommand(backCommand);
        addCommand(removeCommand);
        setCommandListener(this);

        this.displayController = displayController;
        this.display = display;
        this.pm = pm;
    }

    public void commandAction(Command c, Displayable d) {
        if (c == backCommand) {
            displayController.showOptions();
        } else if (c == removeCommand) {
            switch (getSelectedIndex()) {
                case 0: //Remove known
                    try {
                        removeKnownCards(null);
                    } catch (DAOException ex) {
                        displayController.showAlert("Database error!", "Sorry! Database excpetion has occured!", ex);
                    }
                    break;
                case 1: //Remove straight
                    try {
                        removeKnownCards(Boolean.TRUE);
                    } catch (DAOException ex) {
                        displayController.showAlert("Database error!", "Sorry! Database excpetion has occured!", ex);
                    }
                    break;
                case 2: //Remove revers
                    try {
                        removeKnownCards(Boolean.FALSE);
                    } catch (DAOException ex) {
                        displayController.showAlert("Database error!", "Sorry! Database excpetion has occured!", ex);
                    }
                    break;
            }
        } else if (c == DisplayController.okCommand) {
            Loading.load(display, new RemoveCards());
        } else if (c == DisplayController.noCommand) {
             displayController.showOptions();
        }
    }


    /**
     * MODE - null means both side
     * <tt>true</tt> means straight way
     * <tt>false</tt> means reversed way
     * @param mode
     * @throws DAOException
     */
    private void removeKnownCards(final Boolean mode) throws DAOException {
        this.mode = mode;
        Loading.load(display, new LoadCards(this));
    }

    private class RemoveCards implements LoadableInterface {



        public void run() throws Exception {
            for (int i = 0; i < knownCards.size(); i++) {
                pm.deleteCard((FlashCard) knownCards.elementAt(i));
            }
        }

        public void afterRun(Exception ex) {
            if (ex == null) {
                displayController.showInfo("Done", knownCards.size() + " cards have been removed");
            } else {
                displayController.showAlert("Database error!", "Sorry! Database excpetion has occured!", ex);
            }
            knownCards.removeAllElements();
        }

    }

    private class LoadCards implements LoadableInterface {

        private final CommandListener commandListener;

        LoadCards(final CommandListener commandListener) {
            this.commandListener = commandListener;
        }

        public void run() throws Exception {
            Enumeration elements = pm.getCards().elements();
            while (elements.hasMoreElements()) {
                FlashCard card = (FlashCard) elements.nextElement();
                if (card.isKnown(mode)) {
                    knownCards.addElement(card);
                }
            }
        }

        public void afterRun(Exception ex) {
            final int numberOfCards = knownCards.size();
            if (numberOfCards > 0) {
                displayController.showConfirmation("Are you sure?", "Do you want to delete " + numberOfCards + " card?", commandListener);
            } else {
                displayController.showInfo("Warning", "None of the cards has been removed!");
            }
        }

    }

}
