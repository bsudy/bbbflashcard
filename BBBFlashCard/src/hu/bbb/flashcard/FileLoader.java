/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard;

import hu.bbb.flashcard.dao.DAOException;
import hu.bbb.flashcard.logic.FileParser;
import hu.bbb.flashcard.model.FlashCard;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import org.netbeans.microedition.lcdui.pda.FileBrowser;
import org.xmlpull.v1.XmlPullParserException;

/**
 *
 * @author envagyok
 */
public class FileLoader implements CommandListener {

    private FileBrowser fileBrowser;
    private Command optionsCommand = new Command("Options", Command.BACK, 99);
    private Command backCommand = new Command("Back", Command.BACK, 99);

    private boolean reset = false;
    private final DisplayController displayController;
    private final CardManager cardManager;

    public FileLoader(final DisplayController displayController, final CardManager cardManager) {
        this.displayController = displayController;
        this.cardManager = cardManager;
    }

    class LoadFile implements LoadableInterface {

        public void run() throws IOException, DAOException, XmlPullParserException {
            if (reset) {
                cardManager.deleteAllCard();
            }
            Vector res = FileParser.parseXML(fileBrowser.getSelectedFile());
            Enumeration enumeration = res.elements();
            while(enumeration.hasMoreElements()) {
                FlashCard flashCard = (FlashCard) enumeration.nextElement();
                cardManager.saveCard(flashCard);
            }
        }

        public void afterRun(Exception exception) {
            fileBrowser = null;
            try {
                if (exception != null) {
                    throw exception;
                }
                displayController.showOptions();
            } catch (DAOException ex) {
                displayController.showAlert("Database error!", "Sorry! Database excpetion has occured!", ex);
            } catch (IOException ex) {
                displayController.showAlert("IO error!", "Sorry! IO excpetion has occured!", ex);
            } catch (XmlPullParserException ex) {
                displayController.showAlert("XML error!", "Sorry! XML excpetion has occured!", ex);
            } catch (Exception ex) {
                displayController.showUnknownError(ex);
            }
        }


    }

    private void loadFile() {
        Loading.load(displayController.getDisplay(), new LoadFile());
    }

    public void showFileBrowser(final boolean reset) {
        this.reset = reset;
        fileBrowser = new FileBrowser(displayController.getDisplay());
        fileBrowser.setTitle("fileBrowser");
        fileBrowser.setCommandListener(this);
        fileBrowser.addCommand(FileBrowser.SELECT_FILE_COMMAND);
        fileBrowser.addCommand(optionsCommand);
        fileBrowser.setFilter(".bfc");
        displayController.getDisplay().setCurrent(fileBrowser);
    }



    public void commandAction(Command c, Displayable s) {
        if (c == FileBrowser.SELECT_FILE_COMMAND) {
            loadFile();
        } else if (c == optionsCommand) {
            fileBrowser = null;
            displayController.showOptions();
        }
    }


}
