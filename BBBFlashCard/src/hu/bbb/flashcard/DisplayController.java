/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;

/**
 *
 * @author envagyok
 */
public class DisplayController {

    private final Display display;
    private final FlashcardMIDlet flashcardMIDlet;
    private final Displayable removeScreen;
    private final Displayable optionsScreen;
    private final FileLoader fileLoader;

    public static final Command okCommand = new Command("Yes", Command.OK, 1);
    public static final Command noCommand = new Command("No", Command.CANCEL, 1);

    public DisplayController(final FlashcardMIDlet flashcardMIDlet, final CardManager cardManager) {
        this.flashcardMIDlet = flashcardMIDlet;
        this.display = flashcardMIDlet.getDisplay();
        this.removeScreen = new RemoveScreen(display, this, cardManager);
        this.optionsScreen = new OptionsScreen(this, flashcardMIDlet);
        this.fileLoader = new FileLoader(this, cardManager);
    }


    void showOptions() {
        display.setCurrent(optionsScreen);
    }

    void showFileBrowser(boolean reset) {
        fileLoader.showFileBrowser(reset);
    }

    void showRemoveScreen() {
        display.setCurrent(removeScreen);
    }

    public void showUnknownError(Throwable ex) {
        showAlert("Unknown error!", "Sorry! Unknown excpetion has occured!", optionsScreen, ex);
    }
    
    public void showAlert(final String title, final String message) {
        showAlert(title, message, optionsScreen);
    }

    public void showAlert(final String title, final String message, final Throwable error) {
        showAlert(title, message, optionsScreen, error);
    }

    public void showAlert(String title, String message, Displayable nextDisplayable) {
        Alert alert = new Alert(title);
        alert.setString(message);
        alert.setTimeout(Alert.FOREVER);
        display.setCurrent(alert, nextDisplayable);
    }

    public void showAlert(final String title, final String message, final Displayable nextDisplayable, final Throwable error) {
        final Alert alert = new Alert(title);
        String tmpMessage = message;
        if (error != null) {
            tmpMessage += " Error: " + error.getMessage();
        }
        alert.setString(tmpMessage);

        alert.setTimeout(Alert.FOREVER);
        display.setCurrent(alert, nextDisplayable);
    }

    public void showConfirmation(final String title, final String message, final CommandListener listener) {
        Alert dialog = new Alert(title, message, null, AlertType.CONFIRMATION);
        dialog.addCommand(okCommand);
        dialog.addCommand(noCommand);
        dialog.setCommandListener(listener);
        display.setCurrent(dialog);
    }

    public void showInfo(final String title, final String message) {
        Alert alert = new Alert(title, message, null, AlertType.INFO);
        alert.setTimeout(3000);
        display.setCurrent(alert, optionsScreen);
    }

    Displayable getOptions() {
        return optionsScreen;
    }

    Display getDisplay() {
        return display;
    }
}
