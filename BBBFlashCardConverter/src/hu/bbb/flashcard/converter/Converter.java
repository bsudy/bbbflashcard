/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard.converter;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import jxl.read.biff.BiffException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author bsudy
 */
public class Converter {

    public static final String DEFAULT_EXTENSION = ".bfc";

    public enum FileFormat {
        EXCEL,
        OPENDOCUMENT;

        public static FileFormat parseFilename(final String filename) {
            final int dotPos = filename.lastIndexOf(".");
            final String extension = filename.substring(dotPos);
            if (".xls".equalsIgnoreCase(extension)) {
                return EXCEL;
            }
            return OPENDOCUMENT;
        }
    }

    public static void convert(final File inputFile, final File outputFile) throws ParserConfigurationException, IOException, BiffException, TransformerException {
        convert(FileFormat.parseFilename(inputFile.getAbsolutePath()), inputFile, outputFile);
    }

    public static void convert(FileFormat inputFileFormat, final File inputFile, final File outputFile) throws ParserConfigurationException, IOException, BiffException, TransformerException {
        final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        final Document xmlDoc = documentBuilder.newDocument();

        final Element rootElement = xmlDoc.createElement("flashcards");
        xmlDoc.appendChild(rootElement);

        switch(inputFileFormat) {
            case EXCEL:
                ExcelConverter.convertExcel(inputFile, rootElement, xmlDoc);    
                break;
            case OPENDOCUMENT:
                OpenDocumentConverter.convertOpenDocument(inputFile, rootElement, xmlDoc);
                break;
                default: throw new UnsupportedOperationException();
        }

        writeXmlFile(xmlDoc, outputFile);

    }

    public static void writeXmlFile(final Document doc, final File outputFile) throws TransformerException {
        // Prepare the DOM document for writing
        final Source source = new DOMSource(doc);

        // Prepare the output file

        final Result result = new StreamResult(outputFile);

        // Write the DOM document to the file
        final Transformer xformer = TransformerFactory.newInstance().newTransformer();
        xformer.transform(source, result);
    }

}
