/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard.converter;

import java.io.File;
import java.io.IOException;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author envagyok
 */
public class ExcelConverter {

    /**
     * @param args the command line arguments
     */
    public static void convertExcel(final File file, final Element rootElement, final Document xmlDoc) throws IOException, BiffException {

        Workbook workbook = Workbook.getWorkbook(file);
        Sheet sheet = workbook.getSheet(0);
        for (int row = 0; row < sheet.getRows(); row++) {
            Cell sideACell = sheet.getCell(0, row);
            Cell sideBCell = sheet.getCell(1, row);
            String sideA = sideACell.getContents();
            String sideB = sideBCell.getContents();
            if (sideA != null && sideB != null) {
                System.out.println("New flashcard:" + sideA + "#" + sideB);
                Element flashcardNode = xmlDoc.createElement("flashcard");
                Element sideANode = xmlDoc.createElement("sidea");
                Element sideBNode = xmlDoc.createElement("sideb");
                sideANode.appendChild(xmlDoc.createTextNode(sideA));
                sideBNode.appendChild(xmlDoc.createTextNode(sideB));
                flashcardNode.appendChild(sideANode);
                flashcardNode.appendChild(sideBNode);
                rootElement.appendChild(flashcardNode);
            }
        }
    }


}
