/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard.converter;

import java.io.File;
import java.io.IOException;


import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author envagyok
 */
public class OpenDocumentConverter {

    /**
     * @param args the command line arguments
     */
    public static void convertOpenDocument(final File file, final Element rootElement, final Document xmlDoc) throws IOException {
        final Sheet sheet = SpreadSheet.createFromFile(file).getSheet(0);
        for (int row = 0; row < sheet.getRowCount(); row++) {
            final MutableCell sideACell = sheet.getCellAt(0, row);
            final MutableCell sideBCell = sheet.getCellAt(1, row);

            final String sideA = sideACell.getTextValue();
            final String sideB = sideBCell.getTextValue();
            if (sideA != null && sideB != null && !sideA.isEmpty() && !sideB.isEmpty()) {
                System.out.println("New flashcard:" + sideA + "#" + sideB);
                final Element flashcardNode = xmlDoc.createElement("flashcard");
                final Element sideANode = xmlDoc.createElement("sidea");
                final Element sideBNode = xmlDoc.createElement("sideb");
                sideANode.appendChild(xmlDoc.createTextNode(sideA));
                sideBNode.appendChild(xmlDoc.createTextNode(sideB));
                flashcardNode.appendChild(sideANode);
                flashcardNode.appendChild(sideBNode);
                rootElement.appendChild(flashcardNode);
            }
        }
    }

}
