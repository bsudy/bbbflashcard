/*
 *  This file is part of BBBFlashCard.
 *
 *  BBBFlashCard is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BBBFlashCard is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BBBFlashCard.  If not, see <http://www.gnu.org/licenses/>.
 */
package hu.bbb.flashcard.converter;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import jxl.read.biff.BiffException;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

/**
 * The application's main frame.
 */
public class ConverterView extends FrameView {

    public ConverterView(SingleFrameApplication app) {
        super(app);

        initComponents();
        inputFileChooser.addChoosableFileFilter(excelFileFilter);
        inputFileChooser.addChoosableFileFilter(opendocumentFileFilter);
        inputFileChooser.setFileFilter(supportedFileFilter);

        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {

            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String) (evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer) (evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = ConverterApp.getApplication().getMainFrame();
            aboutBox = new ConverterAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        ConverterApp.getApplication().show(aboutBox);
    }

    @Action
    public void showInputFileChooser() {
        ConverterApp.getApplication().show(inputFileChooserDialog);
    }

    @Action
    public void showOutputFileChooser() {
        ConverterApp.getApplication().show(outputFileChooserDialog);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        inputFileField = new javax.swing.JTextField();
        inputFileChooseButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        outputFileField = new javax.swing.JTextField();
        outputFileChooseButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();
        inputFileChooserDialog = new javax.swing.JDialog();
        inputFileChooser = new javax.swing.JFileChooser();
        outputFileChooserDialog = new javax.swing.JDialog();
        outputFileChooser = new javax.swing.JFileChooser();

        mainPanel.setName("mainPanel"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(hu.bbb.flashcard.converter.ConverterApp.class).getContext().getResourceMap(ConverterView.class);
        inputFileField.setText(resourceMap.getString("inputFileField.text")); // NOI18N
        inputFileField.setName("inputFileField"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(hu.bbb.flashcard.converter.ConverterApp.class).getContext().getActionMap(ConverterView.class, this);
        inputFileChooseButton.setAction(actionMap.get("showInputFileChooser")); // NOI18N
        inputFileChooseButton.setText(resourceMap.getString("inputFileChooseButton.text")); // NOI18N
        inputFileChooseButton.setName("inputFileChooseButton"); // NOI18N

        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        outputFileField.setText(resourceMap.getString("outputFileField.text")); // NOI18N
        outputFileField.setName("outputFileField"); // NOI18N

        outputFileChooseButton.setAction(actionMap.get("showOutputFileChooser")); // NOI18N
        outputFileChooseButton.setText(resourceMap.getString("outputFileChooseButton.text")); // NOI18N
        outputFileChooseButton.setName("outputFileChooseButton"); // NOI18N

        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        jButton1.setAction(actionMap.get("convert")); // NOI18N
        jButton1.setText(resourceMap.getString("jButton1.text")); // NOI18N
        jButton1.setName("jButton1"); // NOI18N

        jButton2.setAction(actionMap.get("quit")); // NOI18N
        jButton2.setText(resourceMap.getString("jButton2.text")); // NOI18N
        jButton2.setName("jButton2"); // NOI18N

        jLabel3.setFont(resourceMap.getFont("jLabel3.font")); // NOI18N
        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(30, 30, 30)
                        .addComponent(inputFileField, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(inputFileChooseButton))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(30, 30, 30)
                        .addComponent(outputFileField, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(outputFileChooseButton))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(inputFileChooseButton)
                    .addComponent(inputFileField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(outputFileChooseButton)
                    .addComponent(outputFileField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(34, 34, 34)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jLabel3)
                    .addComponent(jButton1))
                .addContainerGap(54, Short.MAX_VALUE))
        );

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 250, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        inputFileChooserDialog.setName("inputFileChooserDialog"); // NOI18N

        inputFileChooser.setFileFilter(supportedFileFilter);
        inputFileChooser.setName("inputFileChooser"); // NOI18N
        inputFileChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputFileChooserActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout inputFileChooserDialogLayout = new javax.swing.GroupLayout(inputFileChooserDialog.getContentPane());
        inputFileChooserDialog.getContentPane().setLayout(inputFileChooserDialogLayout);
        inputFileChooserDialogLayout.setHorizontalGroup(
            inputFileChooserDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputFileChooserDialogLayout.createSequentialGroup()
                .addComponent(inputFileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        inputFileChooserDialogLayout.setVerticalGroup(
            inputFileChooserDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputFileChooserDialogLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(inputFileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        outputFileChooserDialog.setName("outputFileChooserDialog"); // NOI18N

        outputFileChooser.setDialogType(javax.swing.JFileChooser.SAVE_DIALOG);
        outputFileChooser.setName("outputFileChooser"); // NOI18N
        outputFileChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputFileChooserActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout outputFileChooserDialogLayout = new javax.swing.GroupLayout(outputFileChooserDialog.getContentPane());
        outputFileChooserDialog.getContentPane().setLayout(outputFileChooserDialogLayout);
        outputFileChooserDialogLayout.setHorizontalGroup(
            outputFileChooserDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(outputFileChooserDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(outputFileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        outputFileChooserDialogLayout.setVerticalGroup(
            outputFileChooserDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, outputFileChooserDialogLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(outputFileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents


    private FileFilter excelFileFilter = new MyFileFilter(new String[] {"xls"}, "Excel spreadsheet");
    private FileFilter opendocumentFileFilter = new MyFileFilter(new String[] {"ods"}, "Opendocument spreadsheet");
    private FileFilter supportedFileFilter = new MyFileFilter(new String[] {"xls", "ods"}, "Supported files");


    class MyFileFilter extends FileFilter {

        private final String[] okFileExtensions;
        private final String description;

        public MyFileFilter(String[] okFileExtensions, String description) {
            this.okFileExtensions = okFileExtensions;
            this.description = description;
        }


//        = new String[]{"xls", "ods"};

        @Override
        public boolean accept(File file) {
            if (file.isDirectory()) return true;
            for (String extension : okFileExtensions) {
                if (file.getName().toLowerCase().endsWith(extension)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public String getDescription() {
            String ret = description + " (";
            boolean first = true;
            for (String extension : okFileExtensions) {
                if (!first) {
                    ret += ", ";
                }
                first = false;
                ret += extension;
            }
            ret += ")";
            return ret;
        }
    };

    private void inputFileChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputFileChooserActionPerformed
        // TODO add your handling code here:
        if ("ApproveSelection".equalsIgnoreCase(evt.getActionCommand())) {
            File file = inputFileChooser.getSelectedFile();
            if (file != null) {
                System.out.println("Selected file:" + file.getAbsolutePath());
                inputFileField.setText(file.getAbsolutePath());
                if (outputFileField.getText() == null || outputFileField.getText().isEmpty()) {
                    outputFileField.setText(file.getAbsolutePath() + Converter.DEFAULT_EXTENSION);
                }
            }
        }
        inputFileChooserDialog.setVisible(false);
    }//GEN-LAST:event_inputFileChooserActionPerformed

    private void outputFileChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outputFileChooserActionPerformed
        // TODO add your handling code here:
        if ("ApproveSelection".equalsIgnoreCase(evt.getActionCommand())) {
            File file = outputFileChooser.getSelectedFile();
            if (file != null) {
                System.out.println("Selected file:" + file.getAbsolutePath());
                outputFileField.setText(file.getAbsolutePath());
            }
        }
        outputFileChooserDialog.setVisible(false);
    }//GEN-LAST:event_outputFileChooserActionPerformed

    @Action
    public void convert() {
        File inputFile = new File(inputFileField.getText());
        File outputFile = new File(outputFileField.getText());

        if (!inputFile.isFile() || !inputFile.canRead()) {
            JOptionPane.showMessageDialog(mainPanel, "File is not exist or readable!", "No file!", JOptionPane.ERROR_MESSAGE);
            return;
        }


        if (outputFile.isDirectory()) {
            JOptionPane.showMessageDialog(mainPanel, "Please choose a file!", "No file!", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (outputFile.isFile() && outputFile.canWrite()) {
            int i = JOptionPane.showConfirmDialog(mainPanel, "Do you want to override the file?", "File override", JOptionPane.YES_NO_OPTION);
            if (i == 1) {
                return;
            }
        }
        try {
            Converter.convert(inputFile, outputFile);
        } catch (ParserConfigurationException ex) {
            JOptionPane.showMessageDialog(mainPanel, "XML generation error! " + ex.getMessage(), "XML Error!", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            return;
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(mainPanel, "IO error! " + ex.getMessage(), "IO Error!", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            return;
        } catch (BiffException ex) {
            JOptionPane.showMessageDialog(mainPanel, "Excel reading error! " + ex.getMessage(), "Excel Error!", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            return;
        } catch (TransformerException ex) {
            JOptionPane.showMessageDialog(mainPanel, "XML generation error! " + ex.getMessage(), "XML Error!", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            return;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(mainPanel, "Unexpected error! " + ex.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            return;
        }

        inputFileField.setText("");
        outputFileField.setText("");

        JOptionPane.showMessageDialog(mainPanel, "Success! ", "Success!", JOptionPane.INFORMATION_MESSAGE);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton inputFileChooseButton;
    private javax.swing.JFileChooser inputFileChooser;
    private javax.swing.JDialog inputFileChooserDialog;
    private javax.swing.JTextField inputFileField;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JButton outputFileChooseButton;
    private javax.swing.JFileChooser outputFileChooser;
    private javax.swing.JDialog outputFileChooserDialog;
    private javax.swing.JTextField outputFileField;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    // End of variables declaration//GEN-END:variables
    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private JDialog aboutBox;
}
